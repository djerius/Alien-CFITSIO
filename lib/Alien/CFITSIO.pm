package Alien::CFITSIO;

# ABSTRACT: Build and Install the CFITSIO library

use strict;
use warnings;

use base qw( Alien::Base );

our $VERSION = 'v4.4.0.3';
use constant
  CFITSIO_VERSION => #{{ my @V = $dist->version =~ /(\d+)(?:[.]|$)/g; pop @V; $V[0] + ( $V[1] + $V[2] / 100 ) / 100 }}#;

  1;

# COPYRIGHT

__END__

=pod

=for stopwords
metacpan


=head1 SYNOPSIS

  use Alien::CFITSIO;

=head1 DESCRIPTION

This module finds or builds the L<CFITSIO|https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html> library.  It supports CFITSIO
version #{{ my @V = $dist->version =~ /(\d+)(?:[.]|$)/g; pop
@V; join q{.}, @V;}}#.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

=head1 INSTALLATION

The environment variables C<ALIEN_CFITSIO_EXACT_VERSION> and
C<ALIEN_CFITSIO_ATLEAST_VERSION> may be used during installation to
install a specific version of CFITSIO or any version greater or equal
to a specific version.

By default, B<Alien::CFITSIO> will install CFITSIO version #{{ my @V = $dist->version =~
/(\d+)(?:[.]|$)/g; pop @V; join '.', @V;}}#.
