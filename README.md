# NAME

Alien::CFITSIO - Build and Install the CFITSIO library

# VERSION

version v4.4.0.3

# SYNOPSIS

    use Alien::CFITSIO;

# DESCRIPTION

This module finds or builds the [CFITSIO](https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html) library.  It supports CFITSIO
version 4.4.0.

# USAGE

Please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien%3A%3ABuild%3A%3AManual%3A%3AAlienUser) (or equivalently on [metacpan](https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod)).

# INSTALLATION

The environment variables `ALIEN_CFITSIO_EXACT_VERSION` and
`ALIEN_CFITSIO_ATLEAST_VERSION` may be used during installation to
install a specific version of CFITSIO or any version greater or equal
to a specific version.

By default, **Alien::CFITSIO** will install CFITSIO version 4.4.0.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-alien-cfitsio@rt.cpan.org  or through the web interface at: https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-CFITSIO

## Source

Source is available at

    https://gitlab.com/djerius/alien-cfitsio

and may be cloned from

    https://gitlab.com/djerius/alien-cfitsio.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2022 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
